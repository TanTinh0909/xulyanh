import cv2

img = cv2.imread('7.jpg',cv2.IMREAD_COLOR)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
contours,h = cv2.findContours(thresh,1,2)
largest_rectangle = [0,0]
for cnt in contours:
    approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
    if len(approx) == 4:
        area = cv2.contourArea(cnt)
        if area > largest_rectangle[0]:
            largest_rectangle = [cv2.contourArea(cnt),cnt,approx]

x,y,w,h = cv2.boundingRect(largest_rectangle[1])

image = img[y:y+h,x:x+w]
cv2.drawContours(img,[largest_rectangle[1]],0,(1,0,255),5)
cv2.imshow('return',img)
cv2.waitKey(0)
cv2.destroyAllWindows()